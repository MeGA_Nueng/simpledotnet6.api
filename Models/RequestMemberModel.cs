﻿
using System;

namespace SimpleDotnet6.Api.Models
{
    public class RequestMemberModel
    {
        public string Asid { get; set; }
        public string Email { get; set; }
        public string TelNo { get; set; }
        public string FbName { get; set; }
    }
}
