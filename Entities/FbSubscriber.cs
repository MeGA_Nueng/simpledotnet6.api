﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class FbSubscriber
    {
        public string FbName { get; set; } = null!;
        public long FbpageId { get; set; }
        public string Psid { get; set; } = null!;
        public string? Asid { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? LastTimeActive { get; set; }
    }
}
