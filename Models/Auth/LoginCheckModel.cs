﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models.Auth
{
    public class LoginCheckModel
    {
        public string accessToken { get; set; }
        public string provider { get; set; }
    }
}
