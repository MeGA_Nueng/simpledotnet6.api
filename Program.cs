using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NLog.Web;
using System.Globalization;
using System.Text;
using SimpleDotnet6.Api.AutoMapper;
using SimpleDotnet6.Api.Entities;
using SimpleDotnet6.Api.Helpers;
using SimpleDotnet6.Api.Models.Config;
using SimpleDotnet6.Api.Repositories;
using SimpleDotnet6.Api.Repositories.Interface;
using SimpleDotnet6.Api.Services;
using SimpleDotnet6.Api.Services.Interface;

var logger = NLogBuilder.ConfigureNLog(Path.Combine("Configurations", "nlog.config")).GetCurrentClassLogger();
try
{
    var builder = WebApplication.CreateBuilder(args);
    var webHostBuilder = builder.WebHost;
    var environment = builder.Environment.EnvironmentName;
    builder.Logging.ClearProviders().SetMinimumLevel(LogLevel.Trace);
    webHostBuilder.ConfigureAppConfiguration((hostingContext, config) =>
    {
        config
              .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
              .AddJsonFile(Path.Combine("Configurations", "appsettings.json"), true, true)
              .AddJsonFile(Path.Combine("Configurations", $"appsettings.{environment}.json"), true, true)
              //.AddEncryptedJsonFile(Path.Combine("SystemConfigurations", "Configuration.json"), true, true)
              .AddEnvironmentVariables();
    })
    .UseNLog();

    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.Configure<TokenConfigModel>(builder.Configuration.GetSection("TokenSettings"));
    builder.Services.Configure<AccountPolicyConfigModel>(builder.Configuration.GetSection("AccountPolicySettings"));
    builder.Services.Configure<SmtpEmailConfigModel>(builder.Configuration.GetSection("SmtpEmailSettings"));
    builder.Services.Configure<ClearConsumerSettingsModel>(builder.Configuration.GetSection("ClearConsumerSettings"));

    //Service 
    builder.Services.AddScoped<IUserService, UserService>();
    builder.Services.AddScoped<IAuthService, AuthService>();

    //Repo
    builder.Services.AddScoped<IUserRepository, UserRepository>();

    //Context
    string connectionString = Encoding.UTF8.GetString(Convert.FromBase64String(builder.Configuration["ConnectionStrings:CRM.POC"]));
    builder.Services.AddDbContext<CRMPOCContext>(options => options.UseSqlServer(connectionString));
    builder.Services.AddHttpContextAccessor();

    // Helper
    builder.Services.AddScoped<IAuthorizHandler, AuthorizHandler>();

    // autoMapper
    var mapperConfig = new MapperConfiguration(mc =>
    {
        mc.AddProfile(new MappingProfile());
    });
    var mapper = mapperConfig.CreateMapper();
    builder.Services.AddSingleton(mapper);

    builder.Services.AddControllers();

    builder.Services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer();

    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "SimpleDotnet6.Api", Version = "V1" });
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
            Name = "Authorization",
            In = ParameterLocation.Header,
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer"
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            Array.Empty<string>()
                        }
        });
    });

    var app = builder.Build();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c => c.SwaggerEndpoint("./v1/swagger.json", "SimpleDotnet6.Api V1"));
    }

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();

}
catch (Exception ex)
{
    logger.Error(ex);
    throw;
}
finally
{
    NLog.LogManager.Shutdown();
}