﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models
{
    public class QuerySubscriberModel : PaginationModel
    {
        public string FbName { get; set; }
        public string FbpageId { get; set; }
    }
}
