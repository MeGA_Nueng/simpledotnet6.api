﻿using Microsoft.AspNetCore.Mvc;
using SimpleDotnet6.Api.Middlewares;
using SimpleDotnet6.Api.Models.Auth;
using SimpleDotnet6.Api.Services.Interface;

namespace SimpleDotnet6.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : AbstractController
    {
        private readonly IAuthService _authService;
        public AuthController(ILogger<AuthController> logger, IAuthService authService) : base(logger)
        {
            _authService = authService;
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(AuthenticateModel authenticateModel)
        {
            return ResponseResult(await _authService.SignIn(authenticateModel));
        }
        [BearerAuthorize]
        [HttpPost("SignOut")]
        public async Task<IActionResult> SignOut()
        {
            return ResponseResult(await _authService.SignOut());
        }
        [BearerAuthorize]
        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            return ResponseResult(await _authService.ChangePassword(changePasswordModel));
        }
        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            return ResponseResult(await _authService.ResetPassword(resetPasswordModel));
        }
        [BearerAuthorize]
        [HttpPost("ValidateToken")]
        public async Task<IActionResult> ValidateToken()
        {
            return ResponseResult(await _authService.ValidateTokenAsync());
        }
    }
}
