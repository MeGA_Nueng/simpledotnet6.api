﻿using System.Runtime.CompilerServices;
using SimpleDotnet6.Api.Models;

namespace SimpleDotnet6.Api.Services
{
    public abstract class AbstractService
    {
        protected readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        protected AbstractService(ILogger logger, IHttpContextAccessor httpContext, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        protected ResponseModel OK(object data = null)
        {
            return new ResponseModel { Status = 200, Data = data };
        }

        protected ResponseModel NotFound(string message = "", object data = null, [CallerMemberName] string callerMemberName = "")
        {
            _logger.LogWarning($"Not Found : {message}, data: {data}");
            return new ResponseModel { Status = 404, Message = message, Data = data };
        }

        protected ResponseModel BadRequest(string message = "", object data = null, [CallerMemberName] string callerMemberName = "")
        {
            _logger.LogWarning($"BadRequest : {message}, data: {data}");
            return new ResponseModel { Status = 400, Message = message, Data = data };
        }

        protected ResponseModel Unauthorized(string message = "", object data = null, [CallerMemberName] string callerMemberName = "")
        {
            _logger.LogWarning($"Unauthorized : {message}, data: {data}");
            return new ResponseModel { Status = 401, Message = message, Data = data };
        }

        protected ResponseModel Forbidden(string message = "", object data = null, [CallerMemberName] string callerMemberName = "")
        {
            _logger.LogWarning($"Forbidden : {message}, data: {data}");
            return new ResponseModel { Status = 403, Message = message, Data = data };
        }

        protected ResponseModel InternalServerError(Exception ex, [CallerMemberName] string callerMemberName = "")
        {
            _logger.LogError($"Internal server error : {ex.ToString()}");
            return new ResponseModel { Status = 500, Message = ex.Message };
        }
    }
}
