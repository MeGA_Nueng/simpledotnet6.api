﻿using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Models.Auth;

namespace SimpleDotnet6.Api.Services.Interface
{
    public interface IAuthService
    {
        Task<ResponseModel> SignIn(AuthenticateModel authenticateModel);
        Task<ResponseModel> SignOut();
        Task<ResponseModel> ChangePassword(ChangePasswordModel changePasswordModel);
        Task<ResponseModel> ResetPassword(ResetPasswordModel resetPasswordModel);
        Task<ResponseModel> ValidateTokenAsync();
    }
}
