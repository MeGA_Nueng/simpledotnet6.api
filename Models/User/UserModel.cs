﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models.User
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int UserStatusId { get; set; }
        public int UserTypeId { get; set; }

        public UserStatusModel UserStatus { get; set; }
        public UserTypeModel UserType { get; set; }
    }
}
