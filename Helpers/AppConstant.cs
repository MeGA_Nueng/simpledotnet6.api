﻿namespace SimpleDotnet6.Api.Helpers
{
    public class AppConstant
    {
        internal class AuthStatus
        {
            private AuthStatus() { }
            public const string InvalidEmailOrPassword = "AUTH001"; //Invalid email or password
            public const string PasswordExpired = "AUTH002"; //Password has expired
            public const string UserLocked = "AUTH003"; //User has been locked
            public const string TokenRequired = "AUTH004"; //Token is required
            public const string SessionExpired = "AUTH005"; //Session has expired
            public const string TokenExpired = "AUTH006"; //Token has expired
            public const string InvalidToken = "AUTH006"; //Token is invalid
            public const string UserNotActivated = "AUTH007"; //User not Activated
        }
        internal class VerifyCr
        {
            public const string Notfound = "ER001";
            public const string Scanned = "ER002";
        }
    }
}
