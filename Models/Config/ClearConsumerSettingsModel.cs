﻿using System;

namespace SimpleDotnet6.Api.Models.Config
{
    public class ClearConsumerSettingsModel
    {
        public int Notification { set; get; }
        public int MonthCycle { set; get; }
    }

    public class ResultListCycleClearConsumerModel
    {
        public DateTime StartMonth { set; get; }
        public DateTime EndMonth { set; get; }
        public DateTime Notification { set; get; }
        public DateTime Delete { set; get; }
    }
}