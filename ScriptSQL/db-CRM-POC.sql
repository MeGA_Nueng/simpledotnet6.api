USE [CRM.POC]
GO
/****** Object:  Table [dbo].[FB_Customer]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FB_Customer](
	[TelNo] [nvarchar](250) NOT NULL,
	[PSID] [nvarchar](50) NULL,
	[Customer_Name] [nvarchar](250) NOT NULL,
	[FB_Name] [nvarchar](250) NOT NULL,
	[Address] [nvarchar](500) NOT NULL,
	[FBPage_Id] [bigint] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_baf319e5aad33e905abade46859] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FB_Member]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FB_Member](
	[ASID] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[TelNo] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NULL,
	[FB_Name] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_656fadc272d75e60dbf98c50af1] PRIMARY KEY CLUSTERED 
(
	[ASID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FB_Subscriber]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FB_Subscriber](
	[FB_Name] [nvarchar](250) NOT NULL,
	[FBPage_Id] [bigint] NOT NULL,
	[PSID] [nvarchar](50) NOT NULL,
	[ASID] [nvarchar](50) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NULL,
	[LastTimeActive] [datetime2](7) NULL,
 CONSTRAINT [PK_FB_Subscriber] PRIMARY KEY CLUSTERED 
(
	[PSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[CreatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MST_FBPage]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MST_FBPage](
	[Name] [nvarchar](250) NOT NULL,
	[Token] [varchar](255) NOT NULL,
	[PID] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_40530c350a5370dcb73dbb6f93c] PRIMARY KEY CLUSTERED 
(
	[PID] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordHistory]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Salt] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PasswordHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[Phone] [nvarchar](30) NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[Salt] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[PasswordExpired] [datetime] NULL,
	[AccessToken] [nvarchar](max) NULL,
	[AccessTokenExpired] [datetime] NULL,
	[LastFailedSignIn] [datetime] NULL,
	[LastSuccessfulSignIn] [datetime] NULL,
	[ResetPasswordToken] [nvarchar](100) NULL,
	[ResetPasswordExpired] [datetime] NULL,
	[FailedSignIn] [int] NOT NULL,
	[UserStatusId] [int] NOT NULL,
	[UserTypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserStatus]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 09/03/2022 18:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FB_Customer] ON 
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'0943464334', N'6185367821505053', N'ศุภฤกษ์ จิตรามาส', N'Suparurk Evilz', N'2271 Adams Avenue
Laurel, MD 20707', 2, CAST(N'2021-10-07T15:57:45.1666667' AS DateTime2), CAST(N'2021-11-17T16:21:11.2867266' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'9189254060', NULL, N'ชูศักดิ์ ตนยะแหละ', N'Chusak Tonyale', N'4649 Bridge Street
Tulsa, OK 74120', 1, CAST(N'2021-10-07T15:58:12.8133333' AS DateTime2), CAST(N'2021-10-07T15:58:12.8133333' AS DateTime2), NULL, 2)
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'9733313487', NULL, N'ประดิษฐ์ สมบัติตรา', N'Padit Sombuttra', N'777 Jadewood Farms
Boonton, NJ 07005', 1, CAST(N'2021-10-07T15:58:38.8800000' AS DateTime2), CAST(N'2021-10-07T15:58:38.8800000' AS DateTime2), NULL, 3)
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'5418151296', NULL, N'จีรพงษ์ มูเก็ม', N'Gerapong Mugem', N'1279 Skinner Hollow Road
Bend, OR 97702', 1, CAST(N'2021-10-07T15:59:03.8600000' AS DateTime2), CAST(N'2021-10-07T15:59:03.8600000' AS DateTime2), NULL, 4)
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'1111111111', N'6087762291296909', N'Test', N'Supparurk Jitra', N'dsfdsfdf', 2, CAST(N'0933-10-10T00:00:00.0000000' AS DateTime2), CAST(N'2021-11-17T16:21:13.0799417' AS DateTime2), NULL, 5)
GO
INSERT [dbo].[FB_Customer] ([TelNo], [PSID], [Customer_Name], [FB_Name], [Address], [FBPage_Id], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'0254687512', N'4381077638652452', N'anu', N'Anusorn Nueng', N'dsfsg', 2, CAST(N'2021-11-01T00:00:00.0000000' AS DateTime2), CAST(N'2021-11-17T16:21:12.0959347' AS DateTime2), NULL, 6)
GO
SET IDENTITY_INSERT [dbo].[FB_Customer] OFF
GO
INSERT [dbo].[FB_Member] ([ASID], [Email], [TelNo], [CreatedDate], [UpdatedDate], [DeletedDate], [FB_Name]) VALUES (N'2922835421361968', N'szdasd@sdsf.com', N'1111111111', CAST(N'2021-11-17T16:22:49.6862309' AS DateTime2), CAST(N'2021-11-17T16:22:49.6862341' AS DateTime2), NULL, N'Suparurk Evilz')
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Anusorn Nueng', 2, N'4381077638652452', NULL, CAST(N'2021-11-17T16:21:12.0817067' AS DateTime2), CAST(N'2022-01-10T17:09:13.2181772' AS DateTime2), NULL, CAST(N'2021-11-11T13:23:01.0000000' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Supparurk Jit', 2, N'4459563807496717', NULL, CAST(N'2021-11-17T16:21:13.6579727' AS DateTime2), CAST(N'2022-01-10T17:09:14.6576285' AS DateTime2), NULL, CAST(N'2021-10-25T10:57:41.0000000' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'มาลี บุญสูง', 2, N'4748999201853440', NULL, CAST(N'2022-01-10T17:09:10.5728957' AS DateTime2), CAST(N'2022-01-10T17:09:11.1693221' AS DateTime2), NULL, CAST(N'2022-01-10T17:08:21.0000000' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Csp Poc', 2, N'4836484613069871', NULL, CAST(N'2022-01-10T14:22:42.1861741' AS DateTime2), CAST(N'2022-01-11T12:04:42.2565545' AS DateTime2), NULL, CAST(N'2022-01-11T12:04:42.2477260' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Suparurk Evilz', 1, N'5068501953164736', NULL, CAST(N'2021-11-17T16:21:09.8729874' AS DateTime2), CAST(N'2022-01-10T17:09:09.8439785' AS DateTime2), NULL, CAST(N'2021-10-25T11:04:56.0000000' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Supparurk Jitra', 2, N'6087762291296909', NULL, CAST(N'2021-11-17T16:21:13.0693038' AS DateTime2), CAST(N'2022-01-10T17:11:48.7428747' AS DateTime2), NULL, CAST(N'2022-01-10T17:11:48.7397938' AS DateTime2))
GO
INSERT [dbo].[FB_Subscriber] ([FB_Name], [FBPage_Id], [PSID], [ASID], [CreatedDate], [UpdatedDate], [DeletedDate], [LastTimeActive]) VALUES (N'Suparurk Evilz', 2, N'6185367821505053', N'2922835421361968', CAST(N'2021-11-17T16:21:11.2782565' AS DateTime2), CAST(N'2022-01-10T17:09:12.5144573' AS DateTime2), NULL, CAST(N'2021-11-17T13:41:31.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[Logs] ON 
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (4, N'page == [
  {
    "id": "105622685212801",
    "time": 1636360833220,
    "messaging": [
      {
        "sender": {
          "id": "4381077638652452"
        },
        "recipient": {
          "id": "105622685212801"
        },
        "timestamp": 1636360832902,
        "message": {
          "mid": "m_QrNW3Sg8UtdNBBQHogN4kJGh28bVn-g3dC0KIUI-erzn1B3fdCRAH2kMVKxHGyYFN6EtjxDXmkCrWszRB8Xaxw",
          "text": "yuyuyuyu"
        }
      }
    ]
  }
]', NULL)
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (5, N'page == [
  {
    "id": "105622685212801",
    "time": 1636365728433,
    "messaging": [
      {
        "sender": {
          "id": "6185367821505053"
        },
        "recipient": {
          "id": "105622685212801"
        },
        "timestamp": 1636365728048,
        "message": {
          "mid": "m_oFiUrij0Lbz7pKV_GDWNz8PP-ExZaOk-pkUfAUBJHapalNiZWVnymr2bcHrv38VQBcGwQaWK_AyUbLzKUGdinw",
          "text": "สวัสดี"
        }
      }
    ]
  }
]', NULL)
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (6, N'{"object":"page","entry":[{"id":"105622685212801","time":1636366238106,"messaging":[{"sender":{"id":"4381077638652452"},"recipient":{"id":"105622685212801"},"timestamp":1636366237638,"message":{"mid":"m_JMfvY3NOKl8gTfoN09prQ5Gh28bVn-g3dC0KIUI-erwyq8yK3sdoaaPCAeikP0W4XPEj9smYR3IkUr0pA5javQ","text":"what\u0027s up"}}]}]}', CAST(N'2021-11-08T17:10:42.3238021' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (7, N'{"object":"page","entry":[{"id":"105622685212801","time":1636366838997,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636366838805,"message":{"mid":"m_Ym-vsHU_kqlfjt8z2DNtvMPP-ExZaOk-pkUfAUBJHarMV0ErxxsjFxGqbuif4CwVhjugNK63QY8gSAeBR8mo-Q","text":"\u0E42\u0E22\u0E48\u0E27"}}]}]}', CAST(N'2021-11-08T17:20:40.5890277' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (8, N'{"object":"page","entry":[{"id":"105622685212801","time":1636367313539,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636367313295,"message":{"mid":"m_stY1pWJLcVkZLfryAo36qcPP-ExZaOk-pkUfAUBJHaoMTPUpwFmQI7YncJmNlADxoknRhJK0lPzR3fnOFU3xUg","text":"\u0E44\u0E07"}}]}]}', CAST(N'2021-11-08T17:28:35.1932309' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (9, N'{"object":"page","entry":[{"id":"105622685212801","time":1636426452142,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636426451866,"message":{"mid":"m_5XbDhY5kSOqaj7-YqEuKRsPP-ExZaOk-pkUfAUBJHarn0CS7dwAauNbHypePFhzLOfvU_H-UKI0YrQ-sm4KUMA","attachments":[{"type":"image","payload":{"url":"https://scontent.xx.fbcdn.net/v/t39.1997-6/cp0/39178562_1505197616293642_5411344281094848512_n.png?_nc_cat=1\u0026ccb=1-5\u0026_nc_sid=ac3552\u0026_nc_ohc=OBQjeibbFSkAX_6w6Qn\u0026_nc_ad=z-m\u0026_nc_cid=0\u0026_nc_ht=scontent.xx\u0026oh=b10e6a11342032e3b71f129c135eb09e\u0026oe=618E2489","sticker_id":369239263222822}}]}}]}]}', CAST(N'2021-11-09T09:54:16.4161100' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (10, N'{"object":"page","entry":[{"id":"105622685212801","time":1636433989259,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636433989037,"message":{"mid":"m_ND2_RfkEWGKcIYLDa5ix38PP-ExZaOk-pkUfAUBJHao0jm3wwOPBqhBPonekMZsdBCjsoaa4QR9yKysVR8YWJg","text":"\u0E08\u0E49\u0E32"}}]}]}', CAST(N'2021-11-09T11:59:51.3387496' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (11, N'{"object":"page","entry":[{"id":"105622685212801","time":1636434055004,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636434054829,"message":{"mid":"m_UAMOe0Am312oExPjF3zdbcPP-ExZaOk-pkUfAUBJHaoclSDQOvxZ7rCPRoMyoCe1cCKuS6lm5bOrmw4LiGw_Kw","text":"\u0E42\u0E2D\u0E40\u0E04"}}]}]}', CAST(N'2021-11-09T12:00:56.0800801' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (12, N'{"object":"page","entry":[{"id":"105622685212801","time":1636517859322,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636517859134,"message":{"mid":"m_JyOXDJubmQw9SO40q5KU18PP-ExZaOk-pkUfAUBJHarj706qpcHxh-ikJjlYdJDwaasRM3roYKPQo8P8XxBrdQ","text":"\u0E42\u0E2D\u0E40\u0E04"}}]}]}', CAST(N'2021-11-10T11:17:41.4148861' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (13, N'{"object":"page","entry":[{"id":"105622685212801","time":1636518576418,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636518576254,"message":{"mid":"m_Bq1siqehim3jliD8VLiNScPP-ExZaOk-pkUfAUBJHaqPU6FrH2fSlYhuk1-sSgzrQgZ7MAy7uiqfX5Zy2BYAUg","attachments":[{"type":"image","payload":{"url":"https://scontent.xx.fbcdn.net/v/t39.1997-6/cp0/39178562_1505197616293642_5411344281094848512_n.png?_nc_cat=1\u0026ccb=1-5\u0026_nc_sid=ac3552\u0026_nc_ohc=OBQjeibbFSkAX8d9ppA\u0026_nc_ad=z-m\u0026_nc_cid=0\u0026_nc_ht=scontent.xx\u0026oh=3342e5379fa3be5700125135867b71d7\u0026oe=61901EC9","sticker_id":369239263222822}}]}}]}]}', CAST(N'2021-11-10T11:29:38.7836196' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (14, N'{"object":"page","entry":[{"id":"105622685212801","time":1636519288191,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636519288108,"message":{"mid":"m_GyH2KpCG3cVXjnXUVbUQ4sPP-ExZaOk-pkUfAUBJHaotHABgGJVbXt_nK7iq6DQLrV3tNfTeGzz4Dn-wwlM07w","attachments":[{"type":"image","payload":{"url":"https://scontent.xx.fbcdn.net/v/t39.1997-6/cp0/39178562_1505197616293642_5411344281094848512_n.png?_nc_cat=1\u0026ccb=1-5\u0026_nc_sid=ac3552\u0026_nc_ohc=OBQjeibbFSkAX8d9ppA\u0026_nc_ad=z-m\u0026_nc_cid=0\u0026_nc_ht=scontent.xx\u0026oh=3342e5379fa3be5700125135867b71d7\u0026oe=61901EC9","sticker_id":369239263222822}}]}}]}]}', CAST(N'2021-11-10T11:41:29.4354913' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (15, N'{"object":"page","entry":[{"id":"105622685212801","time":1636519296521,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636519296130,"message":{"mid":"m_wqpnm8g5hbaeo2rVMBPlfcPP-ExZaOk-pkUfAUBJHaqIU6asJglcHDeS64UFmIU75oEkIlkXNSTzQQZ0VkzbEg","text":"\u0E2A\u0E38\u0E42\u0E04\u0E48\u0E22\u0E22\u0E22"}}]}]}', CAST(N'2021-11-10T11:41:37.9938592' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (16, N'{"object":"page","entry":[{"id":"105622685212801","time":1636526218534,"messaging":[{"sender":{"id":"4381077638652452"},"recipient":{"id":"105622685212801"},"timestamp":1636526218293,"message":{"mid":"m_Gft1IF1AGXSByvDA7FoH1ZGh28bVn-g3dC0KIUI-erwqvKVH-vR-wVvlMQV4dvZ1XCLfSgVKlxm7MJECEE44Cg","text":"hihi"}}]}]}', CAST(N'2021-11-10T13:37:00.5371834' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (17, N'{"object":"page","entry":[{"id":"105622685212801","time":1636527517858,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636527517731,"message":{"mid":"m_ipDAat8-X59AfZ9Rv8-PmcPP-ExZaOk-pkUfAUBJHaqmCttEYhne40xWkrLhIar-3N9PgCJu-OcgDGYacTQ4HA","attachments":[{"type":"image","payload":{"url":"https://scontent.xx.fbcdn.net/v/t39.1997-6/cp0/39178562_1505197616293642_5411344281094848512_n.png?_nc_cat=1\u0026ccb=1-5\u0026_nc_sid=ac3552\u0026_nc_ohc=OBQjeibbFSkAX8d9ppA\u0026_nc_ad=z-m\u0026_nc_cid=0\u0026_nc_ht=scontent.xx\u0026oh=3342e5379fa3be5700125135867b71d7\u0026oe=61901EC9","sticker_id":369239263222822}}]}}]}]}', CAST(N'2021-11-10T13:58:39.3673135' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (18, N'{"object":"user","entry":[{"id":"105622685212801","time":1636354238545,"messaging":[{"sender":{"id":"4381077638652452"},"recipient":{"id":"105622685212801"},"timestamp":1636354238258,"message":{"mid":"m_qwIRuEMrBoERHIpRdfS68ZGh28bVn-g3dC0KIUI-erx3V2WnyfmvPz5IEnq6ytbgu8oNjgxkxiBvvN3rgkp4bQ","text":"hihihihi"}}]}]}', CAST(N'2021-11-10T16:28:18.9981886' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (19, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AiBGjuakRiMSIn1rkOZ-ouV"}}', CAST(N'2021-11-10T16:29:15.8564309' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (20, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"ARohdl7zCM9u8-kpapueCQL"}}', CAST(N'2021-11-10T16:30:28.1158166' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (21, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AiJNE-HHEKff3De72mwQBV4"}}', CAST(N'2021-11-10T16:49:12.5509907' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (22, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"Azjc9v6630cZMJDkqva1I7b"}}', CAST(N'2021-11-10T16:51:14.1897845' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (23, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AkGSP4MN6q1wEmTL2_UPmJv"}}', CAST(N'2021-11-10T16:54:10.2505879' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (24, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"A3chSJBWfpbSGdb279OKooR"}}', CAST(N'2021-11-10T16:58:59.8567842' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (25, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AZfyVbCv2l7x75FIEmZIEad"}}', CAST(N'2021-11-10T17:04:26.5180926' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (26, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AiuCb40lZlZhJEJTzCWWX5Q"}}', CAST(N'2021-11-10T17:56:20.0260267' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (27, N'Facebook={"access_token":"EAAPvESUjMHIBAGQZBnuhSXkD7HpwESxYxeq0IQKGFnQeZA6OiA1nUJM1ZBzGHWrDKBnJMb1BXgf3rSUsVjHhxa8zusWmEuaSm4ZAIsNw6HZCApZABvbmJZBmhoqyo1CvTnZCaRtXZAbVZBV65bKB9VvIZCZCHcXAI69rrZAv0OrSD3oMCZA0tNruKCxgMjadHhJhZCx1k5RxF5n9Q9dqEc9Gz5NlHROatvNqFx8ZCN6dfbcb6FHV7AHX2M2UaSKlKKszS0ZBDct7fSlUQ4hPyMwZDZD","token_type":"bearer","expires_in":5183586,"auth_type":"rerequest"}', CAST(N'2021-11-10T18:13:10.8702185' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (28, N'Facebook={"error":{"message":"Invalid verification code format.","type":"OAuthException","code":100,"fbtrace_id":"AB6mU2Pq3gYO1me923kKU7H"}}', CAST(N'2021-11-10T18:25:09.9838918' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (29, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"A7DgWWIzJWLGQVExxT1Gt17"}}', CAST(N'2021-11-10T18:33:44.5158294' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (30, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"A_WpgqX6ylVSGrKi37-TcuU"}}', CAST(N'2021-11-10T18:36:51.7330254' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (31, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"AhtkD-jx3BQnoCcmkjFYapC"}}', CAST(N'2021-11-10T19:02:34.4732041' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (32, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"AiyfVR6YFR0_3y40slEziER"}}', CAST(N'2021-11-10T19:05:54.9337448' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (33, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"AoMC9sqsZX1Gh4gnomDcWRy"}}', CAST(N'2021-11-10T19:06:03.3083282' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (34, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"AtRuK92TzRjpimRS5EH52pD"}}', CAST(N'2021-11-10T19:06:18.6873262' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (35, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"Azu9vlLLEBXsHVNJohtfFcg"}}', CAST(N'2021-11-10T19:06:22.7147930' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (36, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"AxA54wRvisVJUHbNw1h_rbp"}}', CAST(N'2021-11-11T13:16:15.1620030' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (37, N'{"object":"page","entry":[{"id":"105622685212801","time":1636611781933,"messaging":[{"sender":{"id":"4381077638652452"},"recipient":{"id":"105622685212801"},"timestamp":1636611781712,"message":{"mid":"m_JfWisMCDttZqdW1ZgkEGI5Gh28bVn-g3dC0KIUI-erzkn_414YD9KNVnRZLhsJj0caaUb5yOTyVMWQf4a190mg","text":"1485951"}}]}]}', CAST(N'2021-11-11T13:23:03.2897639' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (38, N'Facebook={"error":{"message":"Unknown path components: \/oauth","type":"OAuthException","code":2500,"fbtrace_id":"A6UYq0mkog9NZNdi83mXQaI"}}', CAST(N'2021-11-11T13:23:03.8225788' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (39, N'Facebook={"clients":{"AutomaticDecompression":true,"MaxRedirects":null,"ClientCertificates":null,"Proxy":null,"CachePolicy":null,"Pipelined":false,"FollowRedirects":true,"CookieContainer":null,"UserAgent":"RestSharp/106.12.0.0","Timeout":0,"ReadWriteTimeout":0,"UseSynchronizationContext":false,"Authenticator":null,"BaseUrl":"https://graph.facebook.com/v12.0/dialog/oauth?client_id=1107281846677618&redirect_uri=https://computer-stack-poc.web.app/&auth_type=rerequest&scope=email,user_messenger_contact","Encoding":{"BodyName":"utf-8","EncodingName":"Unicode (UTF-8)","HeaderName":"utf-8","WebName":"utf-8","WindowsCodePage":1200,"IsBrowserDisplay":true,"IsBrowserSave":true,"IsMailNewsDisplay":true,"IsMailNewsSave":true,"IsSingleByte":false,"EncoderFallback":{"DefaultString":"?","MaxCharCount":1},"DecoderFallback":{"DefaultString":"?","MaxCharCount":1},"IsReadOnly":true,"CodePage":65001},"PreAuthenticate":false,"ThrowOnDeserializationError":false,"FailOnDeserializationError":true,"ThrowOnAnyError":false,"UnsafeAuthenticatedConnectionSharing":false,"ConnectionGroupName":null,"RemoteCertificateValidationCallback":null,"DefaultParameters":[{"Name":"Accept","Value":"application/json, text/json, text/x-json, text/javascript, application/xml, text/xml","Type":3,"DataFormat":2,"ContentType":null}],"BaseHost":null,"AllowMultipleDefaultParametersWithSameName":false},"response":"{\"error\":{\"message\":\"Unknown path components: \\/oauth\",\"type\":\"OAuthException\",\"code\":2500,\"fbtrace_id\":\"AqrLZMeOQ1-ayM4aOMuMO72\"}}"}', CAST(N'2021-11-11T13:39:09.7386752' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (40, N'{"object":"page","entry":[{"id":"105622685212801","time":1636708499627,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636708499237,"message":{"mid":"m_ns-XAXYhuzY-jlFD31GxFMPP-ExZaOk-pkUfAUBJHaqcd70o8mecxV8LOCKBL1NopIzqU4DQYEWUs7iG0jwl8g","text":"\u0E2A\u0E27\u0E31\u0E2A\u0E14\u0E35"}}]}]}', CAST(N'2021-11-12T16:15:02.9397222' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (41, N'{"object":"page","entry":[{"id":"105622685212801","time":1636708554626,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1636708554463,"message":{"mid":"m_o_nXXwlOsoG0vmfXgFIb3sPP-ExZaOk-pkUfAUBJHapiZVkhrFEhNOXDnqqdKEi0uqE2SvcFysPVOWo3CSnuWw","text":"\u0E42\u0E2D\u0E40\u0E04"}}]}]}', CAST(N'2021-11-12T16:15:55.8344173' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (42, N'{"object":"page","entry":[{"id":"105622685212801","time":1637131104597,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1637131104340,"message":{"mid":"m_N9kGFuRoitwVHMZHO9rWo8PP-ExZaOk-pkUfAUBJHaq8P5gCFifqi1bjwX9POKOMasVXJL4jDM1d0bVjKiUZqQ","text":"555555"}}]}]}', CAST(N'2021-11-17T13:38:26.1758082' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (43, N'{"object":"page","entry":[{"id":"105622685212801","time":1637131292194,"messaging":[{"sender":{"id":"6185367821505053"},"recipient":{"id":"105622685212801"},"timestamp":1637131291990,"message":{"mid":"m_zXE-4Ht4XcqnwwqchLLyUcPP-ExZaOk-pkUfAUBJHaogD7FiUNNzISbowddpxTtObyz4KESFsONhH6QTkY5Uyw","text":"22222"}}]}]}', CAST(N'2021-11-17T13:41:33.6270960' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (44, N'{"object":"page","entry":[{"id":"105622685212801","time":1641799312262,"messaging":[{"sender":{"id":"4836484613069871"},"recipient":{"id":"105622685212801"},"timestamp":1641799311439,"message":{"mid":"m_hrin555rqUaTT2Pk0Brrg16YkL7hBpQbpFSU3CidlvMB9EwavDa4NOXD1sjYskQxBfkQ0K8Eem0dxUvscjiIIg","text":"first message"}}]}]}', CAST(N'2022-01-10T14:21:54.7896964' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (45, N'{"object":"page","entry":[{"id":"105622685212801","time":1641807142648,"messaging":[{"sender":{"id":"4836484613069871"},"recipient":{"id":"105622685212801"},"timestamp":1641807142394,"message":{"mid":"m_fVhXFQFERtuk81EevBejUl6YkL7hBpQbpFSU3CidlvNZCVLV_dcSyGr7KNB5OD6H-mByZ7OIm0Tqjyyg3FnrEQ","text":"Ack"}}]}]}', CAST(N'2022-01-10T16:32:23.6228568' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (46, N'{"object":"page","entry":[{"id":"105622685212801","time":1641809508069,"messaging":[{"sender":{"id":"6087762291296909"},"recipient":{"id":"105622685212801"},"timestamp":1641809507752,"message":{"mid":"m_aL4br2dmOLnj_rPOgdluysbBbBqz0Rwayt6w0a3nUjWCCZl6owrf7qkyHgMrU4gXPL_bW_KeIIalWbQVUYRdEQ","text":"Hi"}}]}]}', CAST(N'2022-01-10T17:11:48.7314457' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (47, N'{"object":"page","entry":[{"id":"105622685212801","time":1641809700148,"messaging":[{"sender":{"id":"4836484613069871"},"recipient":{"id":"105622685212801"},"timestamp":1641809699797,"message":{"mid":"m_iL320NS0wVoYfB-RXR97SV6YkL7hBpQbpFSU3CidlvMxNxQaFR0o5e8F92Lj4OTgA1FoanfD5WgfqBypI1NLTw","text":"ok"}}]}]}', CAST(N'2022-01-10T17:15:00.6506907' AS DateTime2))
GO
INSERT [dbo].[Logs] ([Id], [Message], [CreatedDate]) VALUES (48, N'{"object":"page","entry":[{"id":"105622685212801","time":1641877481428,"messaging":[{"sender":{"id":"4836484613069871"},"recipient":{"id":"105622685212801"},"timestamp":1641877481217,"message":{"mid":"m_rvizHgbW1GI9nfZeRPLikF6YkL7hBpQbpFSU3CidlvMUrzuBWXxTyks_v91lH9Hv6iFpS-6siC5BR49CE1OKwg","text":"ok ka"}}]}]}', CAST(N'2022-01-11T12:04:42.0626276' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[Logs] OFF
GO
SET IDENTITY_INSERT [dbo].[MST_FBPage] ON 
GO
INSERT [dbo].[MST_FBPage] ([Name], [Token], [PID], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'Testwebhookmessenger', N'EAAPvESUjMHIBAJAulzvzeEAuHDFQZCAUWYVYDP4l7rmvt6Rnmf1RCDsktWBv4E95fxZAieZCC00uQBeLPepajKUxbVkeDjBtkNFvyCObXP1VOigL61nrqZCXyo8DNuBMBjH3oyKwo3jEaZCQOfh65DOn0bEf02L1teSOrplK6KufjY4veMLQaqKWHQPiEweoZD', N'101140452362499', CAST(N'2021-11-09T00:00:00.0000000' AS DateTime2), CAST(N'2021-11-09T13:23:00.2966667' AS DateTime2), NULL, 1)
GO
INSERT [dbo].[MST_FBPage] ([Name], [Token], [PID], [CreatedDate], [UpdatedDate], [DeletedDate], [Id]) VALUES (N'Computer Stack POC', N'EAAPvESUjMHIBANwYiTyUqUzOojDRf9mMxpjLILlTkvhjGWIrvOCZBuZCvtkBjRT0AIG2AFqf7yOZBhnWn9uvqt6imXLMVSEE5Jqadw7SkVGjjcL4xLZAXB6Exe7ZAj5SbKugCP7Q1LDxdcmPhFKgsFKZC8po5q5sWZCyM00RIKG5B9IZCzZBy9ZA5uPGmExopqnXEZD', N'105622685212801', CAST(N'2021-10-05T17:28:27.5633333' AS DateTime2), CAST(N'2021-10-05T17:28:27.5633333' AS DateTime2), NULL, 2)
GO
SET IDENTITY_INSERT [dbo].[MST_FBPage] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([Id], [Email], [Phone], [FirstName], [LastName], [ImageUrl], [Salt], [Password], [PasswordExpired], [AccessToken], [AccessTokenExpired], [LastFailedSignIn], [LastSuccessfulSignIn], [ResetPasswordToken], [ResetPasswordExpired], [FailedSignIn], [UserStatusId], [UserTypeId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (40, N'system.admin@computerstack.com', N'1234567890', N'System', N'Admin', NULL, N'Rj+WMgfxTqyObPpGi7f/yw==', N'o3mNcXEaKJNLsWzkHPl8sEDcV76XCR4Io7/w2OGt+xM=', CAST(N'2025-01-01T00:00:00.000' AS DateTime), N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjQwIiwiZW1haWwiOiJzeXN0ZW0uYWRtaW5AY29tcHV0ZXJzdGFjay5jb20iLCJuYW1lIjoiU3lzdGVtIEFkbWluIiwidXNlclR5cGVJZCI6IjEiLCJ1c2VyU3RhdHVzSWQiOiIxIiwibmJmIjoxNjQ2ODI1MjYyLCJleHAiOjE2NDY4Mjg4NjIsImlhdCI6MTY0NjgyNTI2Mn0.0r3U11kQ0DSHJCd6qGsEsaCA1JtqUdxAoOzeUdeX3tA', CAST(N'2022-03-09T19:01:25.927' AS DateTime), NULL, CAST(N'2022-03-09T18:27:42.090' AS DateTime), N'NTU3ZmNjYzUtYWZlMi00YzJhLTlkNjYtNGIyYjA3NmM2ZTll', CAST(N'2022-01-10T14:15:50.290' AS DateTime), 0, 1, 1, CAST(N'2021-11-04T00:00:00.000' AS DateTime), 3, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserStatus] ON 
GO
INSERT [dbo].[UserStatus] ([Id], [Name]) VALUES (1, N'Active')
GO
INSERT [dbo].[UserStatus] ([Id], [Name]) VALUES (2, N'Inactive')
GO
INSERT [dbo].[UserStatus] ([Id], [Name]) VALUES (3, N'Lock')
GO
SET IDENTITY_INSERT [dbo].[UserStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[UserType] ON 
GO
INSERT [dbo].[UserType] ([Id], [Name]) VALUES (1, N'System Admin CSP')
GO
INSERT [dbo].[UserType] ([Id], [Name]) VALUES (2, N'User CSP')
GO
INSERT [dbo].[UserType] ([Id], [Name]) VALUES (3, N'Admin Mana')
GO
INSERT [dbo].[UserType] ([Id], [Name]) VALUES (4, N'User')
GO
SET IDENTITY_INSERT [dbo].[UserType] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ_5d4b15bb16cbd2f87cd0ac235d8]    Script Date: 09/03/2022 18:36:00 ******/
ALTER TABLE [dbo].[FB_Member] ADD  CONSTRAINT [UQ_5d4b15bb16cbd2f87cd0ac235d8] UNIQUE NONCLUSTERED 
(
	[TelNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_User]    Script Date: 09/03/2022 18:36:00 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [IX_User] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FB_Customer] ADD  CONSTRAINT [DF_4c42009394061142aeb69fe20cd]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FB_Customer] ADD  CONSTRAINT [DF_978b1450bf99d26ca02b2501f90]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FB_Member] ADD  CONSTRAINT [DF_5cf40bac4c6d8dfdab5aa41eac0]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FB_Member] ADD  CONSTRAINT [DF_543bcc2698b89d146db38329c9f]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FB_Subscriber] ADD  CONSTRAINT [DF_94202cb94e273e6b2e13143ed91]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FB_Subscriber] ADD  CONSTRAINT [DF_e8309a1eadf90ed206484fbb76e]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[FB_Subscriber] ADD  CONSTRAINT [DF_986ab4f98345ccf13f31161680e]  DEFAULT (getdate()) FOR [LastTimeActive]
GO
ALTER TABLE [dbo].[MST_FBPage] ADD  CONSTRAINT [DF_afcba3867e5a1a1ca056f81e0a5]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MST_FBPage] ADD  CONSTRAINT [DF_00879b1b367d93ca73575c5b63a]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_FailedSignIn]  DEFAULT ((0)) FOR [FailedSignIn]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserStatus] FOREIGN KEY([UserStatusId])
REFERENCES [dbo].[UserStatus] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserStatus]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[UserType] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=active,2=inactive,3=lock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'UserStatusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=system admin(csp), 2=user(mana)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'UserTypeId'
GO
