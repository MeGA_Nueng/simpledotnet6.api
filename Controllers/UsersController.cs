﻿using Microsoft.AspNetCore.Mvc;
using SimpleDotnet6.Api.Middlewares;
using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Services.Interface;

namespace SimpleDotnet6.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : AbstractController
    {
        private readonly IUserService _userService;

        public UsersController(ILogger<UsersController> logger, IUserService userService)
            : base(logger)
        {
            _userService = userService;
        }

        [HttpGet]
        [BearerAuthorize]
        public async Task<IActionResult> Get([FromQuery] QueryUserModel queryModel)
        {
            return ResponseResult(await _userService.GetUsersAsync(queryModel));
        }

        [HttpGet("{userId}")]
        [BearerAuthorize]
        public async Task<IActionResult> Get(int userId)
        {
            return ResponseResult(await _userService.GetUserByIdAsync(userId));
        }
    }
}
