﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models.User
{
    public class UserStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
