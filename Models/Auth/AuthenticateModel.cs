﻿namespace SimpleDotnet6.Api.Models.Auth
{
    public class AuthenticateModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
