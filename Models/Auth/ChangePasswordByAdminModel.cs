﻿namespace SimpleDotnet6.Api.Models.Auth
{
    public class ChangePasswordByAdminModel
    {
        public string NewPassword { get; set; }
        public int UserId { get; set; }
    }
}
