﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class FbCustomer
    {
        public string TelNo { get; set; } = null!;
        public string? Psid { get; set; }
        public string CustomerName { get; set; } = null!;
        public string FbName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public long FbpageId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public long Id { get; set; }
    }
}
