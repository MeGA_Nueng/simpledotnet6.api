﻿namespace SimpleDotnet6.Api.Helpers.Type
{
    public enum BarcodeStatus
    {
        Created = 1,
        Activated = 2,
        ScannedActive = 3
    }
}
