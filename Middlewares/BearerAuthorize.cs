﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SimpleDotnet6.Api.Helpers;
using SimpleDotnet6.Api.Models;
using System.IdentityModel.Tokens.Jwt;

namespace SimpleDotnet6.Api.Middlewares
{
    public class BearerAuthorizeAttribute : TypeFilterAttribute
    {
        public BearerAuthorizeAttribute() : base(typeof(BearerAuthorizeFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class BearerAuthorizeFilter : IAuthorizationFilter
    {
        readonly IAuthorizHandler _authorizHandler;

        public BearerAuthorizeFilter(IAuthorizHandler authorizHandler)
        {
            _authorizHandler = authorizHandler;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!_authorizHandler.IsValidToken())
                context.Result = new UnauthorizedObjectResult(new ResponseModel { Status = 401, Message = "Unauthorized." });

            string authHeader = context.HttpContext.Request.Headers["Authorization"];
            if (authHeader == null)
            {
                return;
            }

            var handler = new JwtSecurityTokenHandler();
            var secureToken = handler.ReadToken(authHeader.Replace("Bearer ", "")) as JwtSecurityToken;

            var authData = secureToken.Claims.First(claim => claim.Type == "auth_data").Value;
            if (string.IsNullOrEmpty(authData))
            {
                context.Result = new ForbidResult();
                return;
            }

            //var authorizeRole = JsonConvert.DeserializeObject<dynamic>(authData);
            //Newtonsoft.Json.Linq.JArray permissions = authorizeRole.Permissions;

            //if (!permissions.Any(x => x.ToString() == _permissionCode))
            //{
            //    context.Result = new ForbidResult();
            //}
        }
    }
}
