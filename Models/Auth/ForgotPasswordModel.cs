﻿namespace SimpleDotnet6.Api.Models.Auth
{
    public class ForgotPasswordModel
    {
        public string Email { get; set; }
    }
}
