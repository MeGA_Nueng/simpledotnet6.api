﻿namespace SimpleDotnet6.Api.Models.Auth
{
    public class UserTokenModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public int UserTypeId { get; set; }
        public int UserStatusId { get; set; }
    }
}
