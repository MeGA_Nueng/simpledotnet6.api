﻿namespace SimpleDotnet6.Api.Models.Config
{
    public class SmtpEmailConfigModel
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public bool EnableSsl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public int Timeout { get; set; }
    }
}
