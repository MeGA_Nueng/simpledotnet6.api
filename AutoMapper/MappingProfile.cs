﻿using AutoMapper;
using SimpleDotnet6.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, Models.User.UserModel>();
            CreateMap<UserStatus, Models.User.UserStatusModel>();
            CreateMap<UserType, Models.User.UserTypeModel>();
        }
    }
}
