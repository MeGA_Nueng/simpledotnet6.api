﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SimpleDotnet6.Api.Entities
{
    public partial class CRMPOCContext : DbContext
    {

        public CRMPOCContext(DbContextOptions<CRMPOCContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FbCustomer> FbCustomers { get; set; } = null!;
        public virtual DbSet<FbMember> FbMembers { get; set; } = null!;
        public virtual DbSet<FbSubscriber> FbSubscribers { get; set; } = null!;
        public virtual DbSet<Log> Logs { get; set; } = null!;
        public virtual DbSet<MstFbpage> MstFbpages { get; set; } = null!;
        public virtual DbSet<PasswordHistory> PasswordHistories { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserStatus> UserStatuses { get; set; } = null!;
        public virtual DbSet<UserType> UserTypes { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FbCustomer>(entity =>
            {
                entity.ToTable("FB_Customer");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(250)
                    .HasColumnName("Customer_Name");

                entity.Property(e => e.FbName)
                    .HasMaxLength(250)
                    .HasColumnName("FB_Name");

                entity.Property(e => e.FbpageId).HasColumnName("FBPage_Id");

                entity.Property(e => e.Psid)
                    .HasMaxLength(50)
                    .HasColumnName("PSID");

                entity.Property(e => e.TelNo).HasMaxLength(250);

                entity.Property(e => e.UpdatedDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<FbMember>(entity =>
            {
                entity.HasKey(e => e.Asid)
                    .HasName("PK_656fadc272d75e60dbf98c50af1");

                entity.ToTable("FB_Member");

                entity.HasIndex(e => e.TelNo, "UQ_5d4b15bb16cbd2f87cd0ac235d8")
                    .IsUnique();

                entity.Property(e => e.Asid)
                    .HasMaxLength(50)
                    .HasColumnName("ASID");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FbName)
                    .HasMaxLength(250)
                    .HasColumnName("FB_Name");

                entity.Property(e => e.TelNo).HasMaxLength(20);

                entity.Property(e => e.UpdatedDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<FbSubscriber>(entity =>
            {
                entity.HasKey(e => e.Psid);

                entity.ToTable("FB_Subscriber");

                entity.Property(e => e.Psid)
                    .HasMaxLength(50)
                    .HasColumnName("PSID");

                entity.Property(e => e.Asid)
                    .HasMaxLength(50)
                    .HasColumnName("ASID");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FbName)
                    .HasMaxLength(250)
                    .HasColumnName("FB_Name");

                entity.Property(e => e.FbpageId).HasColumnName("FBPage_Id");

                entity.Property(e => e.LastTimeActive).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.Property(e => e.Message).IsUnicode(false);
            });

            modelBuilder.Entity<MstFbpage>(entity =>
            {
                entity.HasKey(e => new { e.Pid, e.Id })
                    .HasName("PK_40530c350a5370dcb73dbb6f93c");

                entity.ToTable("MST_FBPage");

                entity.Property(e => e.Pid)
                    .HasMaxLength(50)
                    .HasColumnName("PID");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Token)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<PasswordHistory>(entity =>
            {
                entity.ToTable("PasswordHistory");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Salt).HasMaxLength(100);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.HasIndex(e => e.Email, "IX_User")
                    .IsUnique();

                entity.Property(e => e.AccessTokenExpired).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastFailedSignIn).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.LastSuccessfulSignIn).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.PasswordExpired).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(30);

                entity.Property(e => e.ResetPasswordExpired).HasColumnType("datetime");

                entity.Property(e => e.ResetPasswordToken).HasMaxLength(100);

                entity.Property(e => e.Salt).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserStatusId).HasComment("1=active,2=inactive,3=lock");

                entity.Property(e => e.UserTypeId).HasComment("1=system admin(csp), 2=user(mana)");

                entity.HasOne(d => d.UserStatus)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_UserStatus");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_UserType");
            });

            modelBuilder.Entity<UserStatus>(entity =>
            {
                entity.ToTable("UserStatus");

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.ToTable("UserType");

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
