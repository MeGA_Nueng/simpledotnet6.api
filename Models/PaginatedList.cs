﻿using SimpleDotnet6.Api.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models
{
    public class PaginatedList<T>
    {
        public int CurrentPage { get; private set; }
        public int From { get; private set; }
        public List<T> Data { get; private set; }
        public int PageSize { get; private set; }
        public int To { get; private set; }
        public int TotalCount { get; private set; }
        public int TotalPages { get; private set; }
        public string OrderBy { get; set; }

        public PaginatedList(List<T> items, int count, int currentPage, int pageSize, string orderBy = "")
        {
            CurrentPage = currentPage;
            TotalPages = Convert.ToInt32(Math.Ceiling(double.IsNaN(count / Convert.ToDouble(pageSize)) ? 0 : (count / Convert.ToDouble(pageSize))));
            TotalCount = count;
            PageSize = pageSize;
            From = ((currentPage - 1) * pageSize) + 1;
            To = (From + pageSize) - 1;
            OrderBy = orderBy;

            Data = items;
        }

        public bool HasPreviousPage
        {
            get
            {
                return (CurrentPage > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (CurrentPage < TotalPages);
            }
        }

        public static async Task<PaginatedList<T>> CreateAsync(
            IQueryable<T> source, int currentPage, int pageSize, string sortOn)
        {
            var count = await source.CountAsync();

            string orderBy = sortOn;

            if (!string.IsNullOrEmpty(sortOn))
            {
                var sort = sortOn.Trim().Split(' ');

                var sortBy = sort.First();
                var sortDirection = "ASC";
                if (sort.Length >= 2 && sort.Last().ToUpper() == "DESC")
                    sortDirection = "DESC";

                if (sortDirection.ToUpper() == "ASC")
                {
                    source = source.XOrderBy(sortBy);
                    orderBy = $"{sortBy} ASC";
                }
                else
                {
                    source = source.XOrderByDescending(sortBy);
                    orderBy = $"{sortBy} DESC";
                }
            }

            source = source.Skip(
                (currentPage - 1) * pageSize)
                .Take(pageSize);

            var items = count == 0 ? new List<T>() : await source.ToListAsync();

            return new PaginatedList<T>(items, count, currentPage, pageSize, orderBy);
        }
    }
}
