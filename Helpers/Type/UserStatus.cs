﻿namespace SimpleDotnet6.Api.Helpers.Type
{
    public enum UserStatus
    {
        Active = 1,
        Inactive = 2,
        Locked = 3
    }
}
