﻿using System;

namespace SimpleDotnet6.Api.Models
{
    public class QueryCustomerModel : PaginationModel
    {

        public string FbpageId { get; set; }
        public string CustomerName { get; set; }
        public string FbName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
