﻿using Microsoft.EntityFrameworkCore;
using SimpleDotnet6.Api.Entities;
using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Repositories.Interface;

namespace SimpleDotnet6.Api.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly CRMPOCContext _context;
        public UserRepository(CRMPOCContext context)
        {
            _context = context;
        }
        public async Task<List<User>> GetUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public IQueryable<User> GetUsersAsync(QueryUserModel queryModel)
        {
            IQueryable<User> query = _context.Users
                .Include(d => d.UserStatus)
                .Include(d => d.UserType);

            if (!string.IsNullOrEmpty(queryModel.Email))
            {
                query = query.Where(x => x.Email.Contains(queryModel.Email));
            }
            if (!string.IsNullOrEmpty(queryModel.FirstName))
            {
                query = query.Where(x => x.FirstName.Contains(queryModel.FirstName));
            }
            if (!string.IsNullOrEmpty(queryModel.LastName))
            {
                query = query.Where(x => x.LastName.Contains(queryModel.LastName));
            }
            if (queryModel.UserStatusId.HasValue)
            {
                query = query.Where(x => x.UserStatusId == queryModel.UserStatusId);
            }
            if (queryModel.UserTypeId.HasValue)
            {
                query = query.Where(x => x.UserTypeId == queryModel.UserTypeId);
            }

            return query;
        }

        public async Task<List<User>> GetUsersByIdsAsync(int[] ids)
        {
            return await _context.Users
                .Include(d => d.UserStatus)
                .Include(d => d.UserType).Where(user => ids.Contains(user.Id)).ToListAsync();
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await _context
                .Users
                .Include(d => d.UserStatus)
                .Include(d => d.UserType)
                //.Include(d => d.PasswordHistories)
                .Where(user => user.Email == email).FirstOrDefaultAsync();
        }


        public async Task<User> GetUserByIdAsync(int id)
        {
            return await _context.Users
                .Include(d => d.UserStatus)
                //.Include(d => d.PasswordHistories)
                .Include(d => d.UserType)
                .Where(user => user.Id == id).FirstAsync();
        }

        public async Task<User> GetUserByAccessTokenAsync(string accessToken)
        {
            return await _context.Users
                .Include(d => d.UserStatus)
                .Include(d => d.UserType)
                //.Include(d => d.PasswordHistories)
                .Where(user => user.AccessToken == accessToken).FirstAsync();
        }

        public void UpdateAccessTokenExpired(User user)
        {
            var entity = _context.Users.Find(user.Id);
            _context.Entry(entity).CurrentValues.SetValues(user);
            _context.SaveChanges();
        }

        public async Task<User> UpdateAsync(User user)
        {
            var entity = await _context.Users.FindAsync(user.Id);
            _context.Entry(entity).CurrentValues.SetValues(user);
            await _context.SaveChangesAsync();

            var entity2 = await GetUserByIdAsync(user.Id);

            return entity2;
        }
    }
}
