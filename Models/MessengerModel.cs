﻿
using System.ComponentModel.DataAnnotations;

namespace SimpleDotnet6.Api.Models
{
    public class MessengerModel
    {
        [Required]
        public Recipient recipient { get; set; }
        
        public dynamic message { get; set; }
    }

    public class Recipient
    {
        public string id { get; set; }
    }

    public class MessageBroadcastModel
    {
        public string messaging_type { get; set; }
        public int facebookPageId { get; set; }
        public object message { get; set; }
    }
}
