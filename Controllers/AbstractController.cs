﻿using SimpleDotnet6.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SimpleDotnet6.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AbstractController : ControllerBase
    {
        private readonly ILogger _logger;
        protected AbstractController(ILogger logger) => _logger = logger;

        protected IActionResult ResponseResult(ResponseModel responseModel)
        {
            switch (responseModel.Status)
            {
                case 200:
                    {
                        responseModel.Message = $"Ok. {responseModel.Message}".Trim();
                        return Ok(responseModel);
                    }
                case 400:
                    {
                        responseModel.Message = $"Bad request. {responseModel.Message}".Trim();
                        return BadRequest(responseModel);
                    }
                case 401:
                    {
                        responseModel.Message = $"Unauthorized. {responseModel.Message}".Trim();
                        return Unauthorized(responseModel);
                    }
                case 403:
                    {
                        responseModel.Message = $"Forbidden. {responseModel.Message}".Trim();
                        return StatusCode(403, responseModel);
                    }
                default:
                    {
                        responseModel.Message = $"Internal server error. {responseModel.Message}".Trim();
                        return StatusCode(500, responseModel);
                    }
            }
        }

    }
}
