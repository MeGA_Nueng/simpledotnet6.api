﻿namespace SimpleDotnet6.Api.Models.Config
{
    public class AccountPolicyConfigModel
    {
        public string PasswordRegex { get; set; }
        public int PasswordExpires { get; set; }
        public int EnforcePasswordHistory { get; set; }
        public int AccountLockoutDuration { get; set; }
        public int AccountLockoutUnused { get; set; }
        public int AccountLockoutThreshold { get; set; }
        public string ResetPasswordUrl { get; set; }
        public int ResetPasswordExpires { get; set; }
    }
}
