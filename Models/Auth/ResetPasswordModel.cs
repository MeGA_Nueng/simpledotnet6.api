﻿namespace SimpleDotnet6.Api.Models.Auth
{
    public class ResetPasswordModel
    {
        public string ResetPasswordToken { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
