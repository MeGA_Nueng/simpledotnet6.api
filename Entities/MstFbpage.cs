﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class MstFbpage
    {
        public string Name { get; set; } = null!;
        public string Token { get; set; } = null!;
        public string Pid { get; set; } = null!;
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public long Id { get; set; }
    }
}
