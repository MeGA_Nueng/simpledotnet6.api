﻿using AutoMapper;
using Microsoft.Extensions.Options;
using SimpleDotnet6.Api.Entities;
using SimpleDotnet6.Api.Helpers;
using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Models.Config;
using SimpleDotnet6.Api.Models.User;
using SimpleDotnet6.Api.Repositories.Interface;
using SimpleDotnet6.Api.Services.Interface;

namespace SimpleDotnet6.Api.Services
{
    public class UserService : AbstractService, IUserService
    {
        private readonly IAuthorizHandler _authorizHandler;
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;
        private readonly AccountPolicyConfigModel _accountPolicyConfig;
        private readonly TokenConfigModel _tokenConfig;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(
           ILogger<UserService> logger,
           IAuthorizHandler authorizHandler,
           IUserRepository userRepository,
           IConfiguration configuration,
           IHttpContextAccessor httpContextAccessor,
           IOptionsMonitor<AccountPolicyConfigModel> optionAccountPolicyConfig,
           IOptionsMonitor<TokenConfigModel> optionTokenConfig,
           IMapper mapper)
           : base(logger, httpContextAccessor, configuration)
        {
            _mapper = mapper;
            _authorizHandler = authorizHandler;
            _userRepository = userRepository;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _accountPolicyConfig = optionAccountPolicyConfig.CurrentValue;
            _tokenConfig = optionTokenConfig.CurrentValue;
        }
        public async Task<ResponseModel> GetUserByIdAsync(int userId)
        {
            try
            {
                // Extract UserToken From AuthHeader
                var userTokenModel = _authorizHandler.ExtractUserTokenFromAuthHeader();
                if (userTokenModel == null)
                    return Unauthorized("AUTHHEADER");

                var user = await _userRepository.GetUserByIdAsync(userId);
                if (user != null)
                {
                    return OK(MappingUserModel(user));
                }
                else
                {
                    return BadRequest("NOTFOUND");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<ResponseModel> GetUsersAsync(QueryUserModel queryModel)
        {
            try
            {
                // Extract UserToken From AuthHeader
                var userTokenModel = _authorizHandler.ExtractUserTokenFromAuthHeader();
                if (userTokenModel == null)
                    return Unauthorized("AUTHHEADER");

                var users = _userRepository.GetUsersAsync(queryModel);

                if (userTokenModel.UserTypeId == (int)Helpers.Type.UserType.UserCSP)
                    users = users.Where(x => x.UserTypeId == (int)Helpers.Type.UserType.UserCSP);
                if (userTokenModel.UserTypeId == (int)Helpers.Type.UserType.ManaAdmin)
                    users = users.Where(x => x.UserTypeId == (int)Helpers.Type.UserType.ManaAdmin || x.UserTypeId == (int)Helpers.Type.UserType.User);
                if (userTokenModel.UserTypeId == (int)Helpers.Type.UserType.User)
                    users = users.Where(x => x.UserTypeId == (int)Helpers.Type.UserType.User);

                int CurrentPage = queryModel.CurrentPage;
                int PageSize = queryModel.PageSize;
                if (queryModel.PageSize == 0)
                {
                    CurrentPage = 1;
                    PageSize = users.Count();
                }

                string sortOn;
                if (string.IsNullOrEmpty(queryModel.OrderBy))
                {
                    sortOn = "Id ASC";
                }
                else
                {
                    var split = queryModel.OrderBy.Split(" ");
                    if (split.Length == 1)
                    {
                        sortOn = queryModel.OrderBy + " ASC";
                    }
                    else
                    {
                        sortOn = split[0] + " " + split[1].ToUpper();
                    }
                }


                var pages = await PaginatedList<User>.CreateAsync(users, CurrentPage, PageSize, sortOn);

                var models = new PaginatedList<UserModel>(pages.Data.Select(user => MappingUserModel(user)).ToList(), pages.TotalCount, pages.CurrentPage, pages.PageSize, sortOn);

                return OK(models);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("(Parameter 'propertyName')") || ex.Message.Contains("is not defined for type"))
                {
                    return BadRequest("ORDERBY_IS_NOT_DEFINED_FOR_MODEL");
                }
                return InternalServerError(ex);
            }
        }

        private UserModel MappingUserModel(User user)
        {
            if (user == null)
                return null;

            var mapModel = _mapper.Map<UserModel>(user);

            return mapModel;
        }
    }
}
