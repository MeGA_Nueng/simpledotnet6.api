﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Models.Auth;
using SimpleDotnet6.Api.Models.Config;
using SimpleDotnet6.Api.Repositories.Interface;

namespace SimpleDotnet6.Api.Helpers
{
    public interface IAuthorizHandler
    {
        bool IsValidToken();
        bool IsValidApiToken();
        UserTokenModel ExtractUserTokenFromAuthHeader();
        UserTokenModel ExtractUserTokenFromAuthHeader(string token);
    }

    public class AuthorizHandler : IAuthorizHandler
    {
        private readonly ILogger<AuthorizHandler> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserRepository _userRepository;
        private readonly TokenConfigModel _tokenConfig;

        public AuthorizHandler(
            ILogger<AuthorizHandler> logger,
            IHttpContextAccessor httpContextAccessor,
            IUserRepository userRepository,
            IOptionsMonitor<TokenConfigModel> optionTokenConfig
            )
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
            _tokenConfig = optionTokenConfig.CurrentValue;
        }

        public bool IsValidToken()
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");

                if (string.IsNullOrEmpty(authHeader) || ValidateToken(authHeader.Replace("Bearer ", "")) == false)
                    return false;

                var tbUser = _userRepository.GetUserByAccessTokenAsync(authHeader).Result;
                if (tbUser == null || DateTime.Now > tbUser.AccessTokenExpired)
                    return false;

                var secureToken = handler.ReadToken(authHeader.Replace("Bearer ", "")) as JwtSecurityToken;

                int.TryParse(secureToken.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.UniqueName).Value, out int userId);
                var email = secureToken.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.Email).Value;

                if (userId != tbUser.Id || email != tbUser.Email)
                    return false;

                tbUser.AccessTokenExpired = DateTime.Now.AddMinutes(_tokenConfig.Expires);
                _userRepository.UpdateAccessTokenExpired(tbUser);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public bool IsValidApiToken()
        {
            try
            {
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");

                if (string.IsNullOrEmpty(authHeader))
                    return false;

                var user = _userRepository.GetUserByAccessTokenAsync(authHeader).Result;
                if (user == null || DateTime.Now > user.AccessTokenExpired)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public UserTokenModel ExtractUserTokenFromAuthHeader()
        {
            try
            {
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                return ExtractUserTokenFromAuthHeader(authHeader);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public UserTokenModel ExtractUserTokenFromAuthHeader(string token)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                string authHeader = token;

                if (string.IsNullOrEmpty(authHeader) || !ValidateToken(authHeader.Replace("Bearer ", "")))
                    return null;

                var secureToken = handler.ReadToken(authHeader.Replace("Bearer ", "")) as JwtSecurityToken;

                int.TryParse(secureToken.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.UniqueName).Value, out int userId);
                var email = secureToken.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.Email).Value;
                var userName = secureToken.Claims.First(claim => claim.Type == "name").Value;
                var userStatusId = secureToken.Claims.First(claim => claim.Type == "userStatusId").Value;
                var userTypeId = secureToken.Claims.First(claim => claim.Type == "userTypeId").Value;

                var userTokenModel = new UserTokenModel();
                userTokenModel.UserId = userId;
                userTokenModel.Email = email;
                userTokenModel.UserName = userName;
                userTokenModel.UserStatusId = int.Parse(userStatusId);
                userTokenModel.UserTypeId = int.Parse(userTypeId);
                return userTokenModel;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        private bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                var secret = Encoding.UTF8.GetBytes(Encoding.UTF8.GetString(Convert.FromBase64String(_tokenConfig.SecretKey)));

                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secret),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
