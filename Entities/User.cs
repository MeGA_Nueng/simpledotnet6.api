﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class User
    {
        public int Id { get; set; }
        public string Email { get; set; } = null!;
        public string? Phone { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string? ImageUrl { get; set; }
        public string? Salt { get; set; }
        public string? Password { get; set; }
        public DateTime? PasswordExpired { get; set; }
        public string? AccessToken { get; set; }
        public DateTime? AccessTokenExpired { get; set; }
        public DateTime? LastFailedSignIn { get; set; }
        public DateTime? LastSuccessfulSignIn { get; set; }
        public string? ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordExpired { get; set; }
        public int FailedSignIn { get; set; }
        /// <summary>
        /// 1=active,2=inactive,3=lock
        /// </summary>
        public int UserStatusId { get; set; }
        /// <summary>
        /// 1=system admin(csp), 2=user(mana)
        /// </summary>
        public int UserTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual UserStatus UserStatus { get; set; } = null!;
        public virtual UserType UserType { get; set; } = null!;
    }
}
