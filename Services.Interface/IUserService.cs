﻿using SimpleDotnet6.Api.Models;

namespace SimpleDotnet6.Api.Services.Interface
{
    public interface IUserService
    {
        Task<ResponseModel> GetUsersAsync(QueryUserModel queryModel);
        Task<ResponseModel> GetUserByIdAsync(int userId);
    }
}
