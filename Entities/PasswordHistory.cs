﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class PasswordHistory
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public string Salt { get; set; } = null!;
        public string Password { get; set; } = null!;
        public DateTime CreatedDate { get; set; }
    }
}
