﻿using SimpleDotnet6.Api.Entities;
using SimpleDotnet6.Api.Models;

namespace SimpleDotnet6.Api.Repositories.Interface
{
    public interface IUserRepository
    {
        Task<List<User>> GetUsersAsync();
        IQueryable<User> GetUsersAsync(QueryUserModel queryModel);
        Task<List<User>> GetUsersByIdsAsync(int[] ids);
        Task<User> GetUserByEmailAsync(string email);
        Task<User> GetUserByIdAsync(int id);
        Task<User> GetUserByAccessTokenAsync(string accessToken);
        void UpdateAccessTokenExpired(User user);
        Task<User> UpdateAsync(User user);
    }
}
