﻿namespace SimpleDotnet6.Api.Models.Config
{
    public class TokenConfigModel
    {
        public string SecretKey { get; set; }
        public int Expires { get; set; }
    }
}
