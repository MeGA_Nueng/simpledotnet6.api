﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SimpleDotnet6.Api.Entities
{
    public partial class CRMPOCContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public CRMPOCContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var apiDatabase = Encoding.UTF8.GetString(Convert.FromBase64String(_configuration.GetConnectionString("CRM.POC")));
                optionsBuilder.UseSqlServer(apiDatabase);
            }
        }

        public CRMPOCContext(DbContextOptions<CRMPOCContext> options, IConfiguration configuration = null) : base(options)
        {
            _configuration = configuration;
        }

    }
}
