﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleDotnet6.Api.Models
{
    public class QueryFbPageModel
    {
        [Required]
        public string pid { get; set; }
    }

    public class QueryUserModel : PaginationModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? UserStatusId { get; set; }
        public int? UserTypeId { get; set; }
    }
}
