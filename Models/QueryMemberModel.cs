﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDotnet6.Api.Models
{
    public class QueryMemberModel : PaginationModel
    {
        public string Email { get; set; }
        public string TelNo { get; set; }
        public string FbName { get; set; }

    }
}
