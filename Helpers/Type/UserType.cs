﻿namespace SimpleDotnet6.Api.Helpers.Type
{
    public enum UserType
    {
        SystemAdmin = 1,
        UserCSP = 2,
        ManaAdmin = 3,
        User = 4
    }
}
