﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SimpleDotnet6.Api.Entities;
using SimpleDotnet6.Api.Helpers;
using SimpleDotnet6.Api.Models;
using SimpleDotnet6.Api.Models.Auth;
using SimpleDotnet6.Api.Models.Config;
using SimpleDotnet6.Api.Repositories.Interface;
using SimpleDotnet6.Api.Services.Interface;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace SimpleDotnet6.Api.Services
{
    public class AuthService : AbstractService, IAuthService
    {
        private readonly ILogger<AuthService> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserRepository _userRepository;
        private readonly IAuthorizHandler _authorizHandler;
        private readonly AccountPolicyConfigModel _accountPolicyConfig;
        private readonly TokenConfigModel _tokenConfig;

        public AuthService(
            ILogger<AuthService> logger,
            IHttpContextAccessor httpContextAccessor,
            IUserRepository userRepository,
            IConfiguration configuration,
            IAuthorizHandler authorizHandler,
            IOptionsMonitor<AccountPolicyConfigModel> optionAccountPolicyConfig,
            IOptionsMonitor<TokenConfigModel> optionTokenConfig
            )
            : base(logger, httpContextAccessor, configuration)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
            _authorizHandler = authorizHandler;
            _accountPolicyConfig = optionAccountPolicyConfig.CurrentValue;
            _tokenConfig = optionTokenConfig.CurrentValue;
        }

        public async Task<ResponseModel> SignIn(AuthenticateModel authenticateModel)
        {
            try
            {
                if (string.IsNullOrEmpty(authenticateModel.Email))
                    return BadRequest("EMAIL_IS_REQUIRED");
                if (string.IsNullOrEmpty(authenticateModel.Password))
                    return BadRequest("PASSWORD_IS_REQUIRED");

                _logger.LogInformation($"SignIn():ACCESS, Email: {authenticateModel.Email}");

                var now = DateTime.Now;
                var user = await _userRepository.GetUserByEmailAsync(authenticateModel.Email);
                if (user == null)
                    return Unauthorized(AppConstant.AuthStatus.InvalidEmailOrPassword);  //auth001

                // Validation
                if (user.UserStatusId == (byte)Helpers.Type.UserStatus.Inactive)
                    return Unauthorized(AppConstant.AuthStatus.InvalidEmailOrPassword); //auth001

                //User ที่ไม่ได้มีการใช้งานเกิน 60 วันระบบจะ lock user นั้น ๆ
                if (user.LastSuccessfulSignIn != null && user.LastSuccessfulSignIn?.AddDays(_accountPolicyConfig.AccountLockoutUnused) < now)
                {
                    user.UserStatusId = (byte)Helpers.Type.UserStatus.Locked;
                    await _userRepository.UpdateAsync(user);
                    return Unauthorized(AppConstant.AuthStatus.UserLocked); ////auth003
                }

                if (user.UserStatusId == (byte)Helpers.Type.UserStatus.Locked)
                {
                    if (_accountPolicyConfig.AccountLockoutDuration <= 0)
                        return Unauthorized(AppConstant.AuthStatus.UserLocked); //auth003
                    else
                    {
                        //กำหนดเวลา lock เมื่อ Login ผิด 5 ครั้ง จะต้องรอก่อนจะ Login ได้ใหม่
                        if (user.LastFailedSignIn?.AddMinutes(_accountPolicyConfig.AccountLockoutDuration) >= now)
                        {
                            return Unauthorized(AppConstant.AuthStatus.UserLocked); //auth003
                        }
                    }
                }

                //Password policy (PasswordRegex)
                if (!HashHelper.Validate(authenticateModel.Password, user.Salt, user.Password))
                {
                    user.LastFailedSignIn = DateTime.Now;
                    user.FailedSignIn++;

                    //User ใส่รหัสผ่านผิด 5 ครั้ง ระบบจะทาการ ระงับการใช้งานของ Account [และ] (จะยกเว้นในกรณี User online ใช้งาน อยู่ในระบบ)
                    if ((user.FailedSignIn >= _accountPolicyConfig.AccountLockoutThreshold) && (user.AccessTokenExpired == null || user.AccessTokenExpired.Value.AddMinutes(_tokenConfig.Expires) < now))
                        user.UserStatusId = (byte)Helpers.Type.UserStatus.Locked;
                    await _userRepository.UpdateAsync(user);
                    return Unauthorized(AppConstant.AuthStatus.InvalidEmailOrPassword); //auth001
                }

                //รหัสผ่านต้องมีอายุการใช้ไม่เกิน 60 วัน
                if (user.PasswordExpired == null || user.PasswordExpired.Value < DateTime.Now)
                {
                    //--การแจ้ง Email URL ให้ User ใช้ในการ Reset password : อายุของ URL(Tokens) มีอายุนาน XX นาที
                    user.ResetPasswordToken = Convert.ToBase64String(Encoding.UTF8.GetBytes(Guid.NewGuid().ToString()));
                    user.ResetPasswordExpired = DateTime.Now.AddMinutes(_accountPolicyConfig.ResetPasswordExpires);
                    await _userRepository.UpdateAsync(user);
                    return Unauthorized(AppConstant.AuthStatus.PasswordExpired, new { Token = user.ResetPasswordToken }); //auth002
                }

                // Generate Token
                string accessToken = CreateAccessToken(user);

                // Reset Failed Sing-In & Update Token
                user.FailedSignIn = 0;
                user.LastFailedSignIn = null;
                user.AccessToken = accessToken;
                user.LastSuccessfulSignIn = now;
                user.AccessTokenExpired = now.AddMinutes(_tokenConfig.Expires);

                if (_accountPolicyConfig.AccountLockoutDuration > 0 && user.UserStatusId == (byte)Helpers.Type.UserStatus.Locked)
                {
                    user.UserStatusId = (byte)Helpers.Type.UserStatus.Active;
                }

                await _userRepository.UpdateAsync(user);

                // Return result
                var userModel = new
                {
                    Id = user.Id,
                    Email = user.Email,
                    Phone = user.Phone,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserStatusId = user.UserStatusId,
                    UserTypeId = user.UserTypeId
                };

                return OK(new { User = userModel, Access_token = accessToken });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        public async Task<ResponseModel> SignOut()
        {
            //--กรณี User none active หน้าจอไม่เกิน 30 นาที ให้ระบบ User session timeout ให้ Default ไว้เป็น 30 นาทีในเบื้องต้น
            try
            {
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                if (string.IsNullOrEmpty(authHeader))
                    return Unauthorized("UNAUTHORIZED");

                string accessToken = authHeader.Replace("Bearer ", "").Trim();
                var user = await _userRepository.GetUserByAccessTokenAsync(accessToken);
                if (user == null)
                    return Unauthorized("UNAUTHORIZED");

                // Clear AccessToken
                user.AccessToken = null;
                user.AccessTokenExpired = null;
                user.FailedSignIn = 0;
                user.LastFailedSignIn = null;
                await _userRepository.UpdateAsync(user);

                return OK("SIGN-OUT_COMPLETED");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<ResponseModel> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            try
            {
                var userTokenModel = _authorizHandler.ExtractUserTokenFromAuthHeader();
                if (userTokenModel == null)
                {
                    return Unauthorized("AUTHHEADER");
                }

                // check parameter
                if (string.IsNullOrEmpty(changePasswordModel.CurrentPassword))
                {
                    return BadRequest("CURRENTPASSWORD_IS_REQUIRED");
                }
                if (string.IsNullOrEmpty(changePasswordModel.NewPassword))
                {
                    return BadRequest("CURRENTPASSWORD_IS_REQUIRED");
                }

                //Validate Password Pattern
                if (!RegexHelper.IsMatch(changePasswordModel.NewPassword, _accountPolicyConfig.PasswordRegex))
                {
                    return Unauthorized("THE_PASSWORD_DOES_NOT_MEET_THE_CONDITIONS.");
                }

                //Validate User
                var user = await _userRepository.GetUserByIdAsync(userTokenModel.UserId);
                if (user == null)
                {
                    return Unauthorized("USER_NOT_FOUND");
                }

                //Validate CurrentPassword
                if (!HashHelper.Validate(changePasswordModel.CurrentPassword, user.Salt, user.Password))
                {
                    return Unauthorized("INVALID_CURRENT_PASSWORD");
                }

                //Validate Password History
                //--Password ใหม่ที่ตั้ง จะต้องไม่ซ้ากับของเดิมอย่างน้อย 10 ครั้ง สามารถปรับเปลี่ยนได้ ให้ Default ไว้เป็น 3 ครั้งในเบื้องต้น
                if (_accountPolicyConfig.EnforcePasswordHistory > 0)
                {
                    //var passwordHistories = user.PasswordHistories.OrderByDescending(e => e.CreatedDate).Take(_accountPolicyConfig.EnforcePasswordHistory).ToList();
                    //bool isDuplicate = passwordHistories.Any(e =>
                    //{
                    //    return HashHelper.Validate(changePasswordModel.NewPassword, e.Salt, e.Password);
                    //});

                    //if (isDuplicate)
                    //{
                    //    return BadRequest($"PASSWORD_REPEATED_WITH_THE_LAST_{_accountPolicyConfig.EnforcePasswordHistory}_PASSWORDS");
                    //}
                }

                //Update Password
                string salt = HashHelper.Salt();
                string hashPassword = HashHelper.Create(changePasswordModel.NewPassword, salt);

                user.Password = hashPassword;
                user.Salt = salt;
                user.PasswordExpired = DateTime.Now.AddDays(_accountPolicyConfig.PasswordExpires);
                user.ResetPasswordToken = null;
                user.ResetPasswordExpired = null;
                user.FailedSignIn = 0;
                user.LastFailedSignIn = null;

                //user.PasswordHistories.Add(new PasswordHistory
                //{
                //    UserId = user.Id,
                //    Password = user.Password,
                //    Salt = user.Salt,
                //    CreatedDate = DateTime.Now
                //});

                await _userRepository.UpdateAsync(user);

                return OK("UPDATE");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<ResponseModel> ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            try
            {
                // check parameter
                if (string.IsNullOrEmpty(resetPasswordModel.Email))
                    return BadRequest("EMAIL_IS_REQUIRED");
                if (string.IsNullOrEmpty(resetPasswordModel.Password))
                    return BadRequest("PASSWORD_IS_REQUIRED");
                if (string.IsNullOrEmpty(resetPasswordModel.ResetPasswordToken))
                    return BadRequest("RESETPASSWORDTOKEN_IS_REQUIRED");

                //Validate Password Pattern
                if (!RegexHelper.IsMatch(resetPasswordModel.Password, _accountPolicyConfig.PasswordRegex))
                    return BadRequest("THE_PASSWORD_DOES_NOT_PATTERN");

                //Validate User
                var user = await _userRepository.GetUserByEmailAsync(resetPasswordModel.Email);
                if (user == null)
                    return BadRequest("USER_NOTFOUND");

                //Validate Code Generate Password
                if (string.IsNullOrEmpty(resetPasswordModel.ResetPasswordToken)
                    || string.IsNullOrEmpty(user.ResetPasswordToken)
                    || resetPasswordModel.ResetPasswordToken != user.ResetPasswordToken)
                {
                    return BadRequest("RESET_PASSWORD_TOKEN_IS_INVALID");
                }
                else if (user.ResetPasswordExpired < DateTime.Now)
                {
                    return BadRequest("RESET_PASSWORD_TOKEN_IS_EXPIRED");
                }

                //Validate Password History
                if (_accountPolicyConfig.EnforcePasswordHistory > 0)
                {
                    //var passwordHistories = user.PasswordHistories.OrderByDescending(e => e.CreatedDate).Take(_accountPolicyConfig.EnforcePasswordHistory).ToList();
                    //bool isDuplicate = passwordHistories.Any(e =>
                    //{
                    //    return HashHelper.Validate(resetPasswordModel.Password, e.Salt, e.Password);
                    //});

                    //if (isDuplicate)
                    //    return BadRequest($"PASSWORD_REPEATED_WITH_THE_LAST {_accountPolicyConfig.EnforcePasswordHistory}_PASSWORDS");
                }

                //Update Password
                string salt = HashHelper.Salt();
                string hashPassword = HashHelper.Create(resetPasswordModel.Password, salt);

                user.Password = hashPassword;
                user.Salt = salt;
                user.PasswordExpired = DateTime.Now.AddDays(_accountPolicyConfig.PasswordExpires);
                user.ResetPasswordToken = null;
                user.ResetPasswordExpired = null;
                user.FailedSignIn = 0;
                user.LastFailedSignIn = null;

                if (user.UserStatusId == (byte)Helpers.Type.UserStatus.Locked)
                    user.UserStatusId = (byte)Helpers.Type.UserStatus.Active;

                //user.PasswordHistories.Add(new PasswordHistory
                //{
                //    UserId = user.Id,
                //    Password = user.Password,
                //    Salt = user.Salt,
                //    CreatedDate = DateTime.Now
                //});

                await _userRepository.UpdateAsync(user);

                return OK("UPDATE");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        public async Task<ResponseModel> ValidateTokenAsync()
        {
            try
            {
                string authHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                if (string.IsNullOrEmpty(authHeader))
                    return Unauthorized("AUTHHEADER");

                var user = await _userRepository.GetUserByAccessTokenAsync(authHeader);
                if (user == null)
                    return Unauthorized("AUTHHEADER");
                if (user.AccessTokenExpired <= DateTime.Now)
                    return Unauthorized(AppConstant.AuthStatus.TokenExpired);

                // Generate Token
                string accessToken = CreateAccessToken(user);

                // Reset Failed Sing-In & Update Token
                user.AccessToken = accessToken;
                user.AccessTokenExpired = DateTime.Now.AddMinutes(_tokenConfig.Expires);

                await _userRepository.UpdateAsync(user);

                // Return result
                var userModel = new
                {
                    Id = user.Id,
                    Email = user.Email,
                    Phone = user.Phone,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserStatusId = user.UserStatusId,
                    UserTypeId = user.UserTypeId
                };

                return OK(new { User = userModel, Access_token = accessToken });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private string CreateAccessToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;

            var secret = Encoding.UTF8.GetBytes(Encoding.UTF8.GetString(Convert.FromBase64String(_tokenConfig.SecretKey)));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim("name", $"{user.FirstName} {user.LastName}"),
                    new Claim("userTypeId",  Convert.ToString(user.UserType.Id)),
                    new Claim("userStatusId", Convert.ToString(user.UserStatus.Id)),
                }),
                NotBefore = now,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secret), SecurityAlgorithms.HmacSha256Signature)
            };

            return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
        }
    }
}
