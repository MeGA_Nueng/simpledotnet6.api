﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class Log
    {
        public int Id { get; set; }
        public string? Message { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
