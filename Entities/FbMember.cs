﻿using System;
using System.Collections.Generic;

namespace SimpleDotnet6.Api.Entities
{
    public partial class FbMember
    {
        public string Asid { get; set; } = null!;
        public string? Email { get; set; }
        public string TelNo { get; set; } = null!;
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string FbName { get; set; } = null!;
    }
}
