﻿namespace SimpleDotnet6.Api.Helpers.Type
{
    public enum LogScanStatus
    {
        Duplicated = 1,
        NotFound = 2,
        NotActivate = 3,
        Completed = 4,
        Unknown = 99
    }
}
